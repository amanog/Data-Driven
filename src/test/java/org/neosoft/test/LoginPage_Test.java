package org.neosoft.test;

import org.neosoft.POM.LoginPage_POM;
import org.neosoft.utility.BaseClass;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;

public class LoginPage_Test extends BaseClass
{
	
	LoginPage_POM p = new LoginPage_POM();
	
	@Test(testName="login_admin",groups="Logins")
	public void Login_Admin()
	{
		
		for(int k=0;k<5;k++)
		{
			test = extent.startTest(data[k][2]);
			browser("firefox", "http://professional.demo.orangehrmlive.com/");
			test.log(LogStatus.PASS, "Opened URL");
			p.login_username(driver).sendKeys(data[k][0]);
			test.log(LogStatus.PASS, "Username Entered");
			p.login_password(driver).sendKeys(data[k][1]);
			test.log(LogStatus.PASS, "Pass Entered");
			p.login_BTN(driver).click();
			test.log(LogStatus.PASS, "Clicked Buttons");
			extent.endTest(test);
			driver.close();
		}
		
	
	}

}
